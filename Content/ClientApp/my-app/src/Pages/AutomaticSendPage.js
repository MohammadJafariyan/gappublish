import React, { Component } from 'react'
import { useState } from 'react';
import { MyCaller,CurrentUserInfo } from './../Help/Socket';
import { cookieManager } from './../Help/CookieManager';
import { MyGlobal } from '../Help/MyGlobal';
import Chat, {ChatPannel, gotoBottom, showMultimedia} from "../Components/Chat";
import ChatForm from "../Components/ChatForm";
import {DataHolder} from "../Help/DataHolder";

export default class AutomaticSendPage extends Chat {
    constructor(arg){
        super(arg);

        this.state={};
        CurrentUserInfo.AutomaticSendPage=this;
    }


    

    successCallback(res){
        this.setState({sending:false});
    }

    getAutomaticSendChatsSocketHandlerCallback(res){

        this.setState({sending:false});


        if (!res || !res.Content){
            CurrentUserInfo.LayoutPage.showError('پیام های اتوماتیک خوانده شده صحیح نیست');

            return;
        }
        let arr= res.Content;
        
        if (arr && arr.length>0){
            this.setState({chats:arr});
        }else{

        }
    }


    componentDidMount() {
        MyCaller.Send("GetAutomaticSendChatsSocketHandler");
    }


   

   


    render() {

        return (
            <div className="container ">

<div className='row'>


    <div className="col-6 ">
        <div className="col-md-12 ">

           <ul dir={'rtl'}>
               <li>اتوماتیک از طرف ادمین کم مراجعه ارسال می شود</li>
               <li>از طرف ادمین های مخصوص هر بخش سایت پیغام ارسال می شود</li>
           </ul>
        </div>
        <ChatPannel chats={this.state.chats}   onDelete={(chat)=>{

             this.state.chats.splice(this.state.chats.indexOf(chat),1);
             this.setState({temp:Math.random(),
             chats:this.state.chats})
        }}/>

    </div>
    <div className="col-6  ">


        <div className="form-group">
            <label htmlFor="exampleInputEmail1"> زمان تاخیر به دقیقه </label>
            <input min={0} max={60} type="number" className="form-control" id="exampleInputEmail1"
                   aria-describedby="emailHelp" placeholder="زمان به دقیقه  "
                   value={this.state.delay}
                   onChange={(e) => {
                       if (e.target.value > 60 || e.target.value <= 0) {
                           CurrentUserInfo.LayoutPage.showError('زمان تاخیر فقط مابین 1 الی 60 مورد قبول است');
                           if(e.target.value>60){
                               this.setState({delay: 60});
                           }else{
                               this.setState({delay: 1});

                           }

                       }else{
                           this.setState({delay: e.target.value})

                       }
                   }
                   }
            />

            <small id="emailHelp" className="form-text text-muted">مدت زمانی که کاربر وارد سایت شده است اما
                هیچ پشتیبانی دریافت نکرده است </small>
        </div>
        <div className="form-group">

            <ChatForm
                onPaste={(e) => {
                    this.onPaste(e)
                }} upload={(e) => {
                this.uploadFile(e);
            }} onSubmit={(e) => {
             return    this.submit(e)
            }} onChange={(e) => {
                let multiMedia = showMultimedia(e.target.value);

                if (!multiMedia) {
                    this.setState({text: e.target.value});
                } else {
                    this.setState({text: ""});
                }
            }}
                
              

            />
            {/* <form onSubmit={this.submit}>
                            <input
                                value={this.state.text}
                                onPaste={(e) => {
                                    this.onPaste(e);
                                }}
                                onChange={(e) => {
                                    let multiMedia = showMultimedia(e.target.value);

                                    if (!multiMedia) {
                                        this.setState({ text: e.target.value });
                                    } else {
                                        this.setState({ text: "" });
                                    }
                                }}
                            />

                            <button className="btn btn-default" type="button" onClick={() => {
                                document.getElementById('addFile').click();
                            }}>ارسال فایل</button>
                            <input
                                type="file"
                                id="addFile"
                                hidden="hidden"
                                onChange={(e) => {
                                    this.uploadFile(e);
                                }}
                            />
                        </form>*/}
        </div>
         <button onClick={()=>{
             
             this.saveAutomaticSendChats()
             
  }} type="submit" disabled={this.state.sending} className="btn btn-primary">ثبت </button>
    </div>

</div>
 
  

            </div>
        )
    }

    saveAutomaticSendChats(){
        if(!this.state.chats || !this.state.chats.length){
            CurrentUserInfo.LayoutPage.showError('هیچ پیغامی ثبت نشده است ');
return;
        }
        
        
        this.setState({sending:true});
        
        
        MyCaller.Send("SaveAutomaticSendChatsSocketHandler",{
            chats:this.state.chats
        });
    }
    showAdmins() {
        if (!this.state.admins)
            return <></>;


            return (
                <table>

                    <tbody>
                        {
                            this.state.admins.map((el, i, arr) => {

                                return <tr className="arow" onClick={() => {
                                    this.setState({selectedAdmin:el})
                                }}>
                                    <td>{el.Name}</td>
                    </tr>
            })}
                        </tbody>
                    </table>)
            
    }

    addChat(chat, dontSend) {
       
        let chats = this.getChats();


        chat.Delay=this.state.delay ? this.state.delay : 1;
        chat.DeliverDateTime=new Date();



        chats.push(chat);

        chats.sort((a, b) => {
            if (a.Id < b.Id) {
                return -1;
            }
            if (a.Id > b.Id) {
                return 1;
            }
            // a must be equal to b
            return 0;
        });

        chat.UniqId = chats.length + 1;

        this.setState({ chats: chats });


        if (!this.state.scroll) {
            /*setTimeout(() => {
                gotoBottom("chatPanel");
            }, 500);*/
        }

    }
    submit(e) {
                e.preventDefault();
        if (!this.state.text) return false;
        CurrentUserInfo.ChatPage.setState({ scroll: false });

        this.addChat({ Message: this.state.text },true);

        this.setState({ text: "" });
        return false;
    }

    
}
