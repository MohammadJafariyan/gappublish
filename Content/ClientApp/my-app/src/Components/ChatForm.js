﻿import React,{Component} from "react";
import { CurrentUserInfo } from "../Help/Socket";


export default class ChatForm extends Component {

    constructor(arg) {
        super(arg);
        this.submit=this.submit.bind(this);
        this.state={};
        CurrentUserInfo.ChatForm=this;
    }

    submit(e){
        e.preventDefault();
        
        this.props.onSubmit(e);
        
        this.setState({text:''});

        return false;
    }
    render() {
        return (<div>

            <form onSubmit={this.submit}>
                <input   
                    value={this.state.text}
                    placeholder='جهت ارسال فایل یا پیغام متنی اینجا تایپ نمایید'
                    className='form-control' onChange={(e) =>
                    {

                        
                        this.setState({text:e.target.value});
                    this.props.onChange(e);
                }}
                       onPaste={(e)=>{
                           this.props.onPaste(e);
                       }}

                />

             

              

            </form>
            <button style={{display:'block'}} className='btn btn-default' onClick={()=>{
                document.getElementById('fileUpload').click();
            }}>ارسال فایل</button>
            <input  onChange={(e)=>{
                this.props.upload(e);
            }} id='fileUpload' type='file' hidden='hidden'/>
        </div>);
    }
}