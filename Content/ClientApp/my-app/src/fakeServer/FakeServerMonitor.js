import React, {Component} from 'react'
import {_dispatcher} from './../Help/Dispatcher';
import {CurrentUserInfo} from '../Help/Socket';
import {MyGlobal} from "../Help/MyGlobal";

export default class FakeServerMonitor extends Component {


    constructor(props, context) {
        super(props, context);

        CurrentUserInfo.FakeServerMonitor = this;
    }

    customers = [];
    tags = [];

    ServerCall(name, data) {
        if (name === "DeleteTagById") {

            if (!data.tagId) {
                alert('data.tagId isnull');
            }
            if (!this.tags.filter(f => f.Id === data.tagId)) {
                alert('not found tag');
                return;
            }


            this.tags = this.tags.filter(f => f.Id != data.tagId);


            document.getElementById('getAllTagsForCurrentAdminCallback').click();
        }
    }

    render() {
        if (!MyGlobal.isTestingEnvirement)
        {
            return <></>;
        }
        
        return (
            <div>

                <a href='#' onClick={() => {

                    let customer = {
                        Name: 'کاربر آنلاین' + new Date().getTime(), OnlineStatus: 1, Id: Math.random(),
                        TotalUnRead: (Math.round(Math.random(1, 999999) * 20))

                    };

                    this.customers.push(customer);

                    _dispatcher.dispatch({Name: 'newCustomerOnlineCallback', Content: customer})

                }}>add new Customer</a>

                <br></br>

                <a href='#' onClick={() => {

                    let customer = {
                        Name: 'کاربر آنلاین' + new Date().getTime(), OnlineStatus: 1, Id: Math.random(),
                        TotalUnRead: (Math.round(Math.random(1, 999999) * 20))
                    };


                    this.customers.push(customer);


                    let Content = {};
                    Content.EntityList = this.customers;

                    _dispatcher.dispatch({Name: 'getClientsListForAdminCallback', Content: Content})

                }}>getClientsListForAdminCallback</a>


                <br/>

                <a href='#' onClick={() => {

                    let customer = {
                        Name: 'کاربر آنلاین' + new Date().getTime(), OnlineStatus: 1, Id: Math.random(),
                        TotalUnRead: (Math.round(Math.random(1, 999999) * 20))


                    };


                    this.customers.push(customer);


                    let Content = {};
                    Content.TotalWaitingForAnswerCount = this.customers.length;
                    Content.NotChattedCount = this.customers.length / 2;
                    Content.TotalNewChatReceived = this.customers.length;

                    _dispatcher.dispatch({Name: 'totalUserCountsChangedCallback', Content: Content})

                }}>totalUserCountsChangedCallback</a>


                <br/>

                <a href='#' onClick={() => {

                    let tag = {
                        Name: 'تگ' + new Date().getMinutes(), OnlineStatus: 1, Id: Math.random(),
                        TotalUnRead: (Math.round(Math.random(1, 999999) * 20))


                    };


                    this.tags.push(tag);


                    let Content = {};
                    Content.EntityList = this.tags;

                    _dispatcher.dispatch({Name: 'getAllTagsForCurrentAdminCallback', Content: Content})

                }} id='getAllTagsForCurrentAdminCallback'>getAllTagsForCurrentAdminCallback</a>


                <br/>

                <a href='#' onClick={() => {

                    let tag = {
                        Name: 'تگ' + new Date().getMinutes(), OnlineStatus: 1, Id: Math.random(),
                        TotalUnRead: (Math.round(Math.random(1, 999999) * 20))


                    };


                    this.tags.push(tag);


                    let Content = {};
                    Content.EntityList = this.tags;

                    _dispatcher.dispatch({Name: 'userAddedToTagsCallback', Content: Content})

                }} id='userAddedToTagsCallback'>userAddedToTagsCallback</a>


            </div>
        )
    }
}
